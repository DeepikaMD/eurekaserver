FROM openjdk:8
MAINTAINER Deepika Muralidharan
WORKDIR /Eureka-server
COPY . /Eureka-server
CMD java -jar eureka-0.0.1-SNAPSHOT.jar
EXPOSE 8761